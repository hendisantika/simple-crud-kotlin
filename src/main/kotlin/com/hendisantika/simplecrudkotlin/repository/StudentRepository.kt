package com.hendisantika.simplecrudkotlin.repository

import com.hendisantika.simplecrudkotlin.entity.Student
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Created by IntelliJ IDEA.
 * Project : simple-crud-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/03/21
 * Time: 20.54
 */
@Repository
interface StudentRepository : JpaRepository<Student, String>

