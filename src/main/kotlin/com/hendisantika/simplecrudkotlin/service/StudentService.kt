package com.hendisantika.simplecrudkotlin.service

import com.hendisantika.simplecrudkotlin.entity.Student
import com.hendisantika.simplecrudkotlin.util.ListDataPagination

/**
 * Created by IntelliJ IDEA.
 * Project : simple-crud-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/03/21
 * Time: 20.56
 */

interface StudentService {

    fun create(student: Student): Student

    fun get(id: String): Student

    fun update(id: String, updateStudent: Student): Student

    fun delete(id: String)

    fun list(listDataPagination: ListDataPagination): List<Student>
}