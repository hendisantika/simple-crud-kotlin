package com.hendisantika.simplecrudkotlin.service

import com.hendisantika.simplecrudkotlin.entity.Student
import com.hendisantika.simplecrudkotlin.repository.StudentRepository
import com.hendisantika.simplecrudkotlin.util.ListDataPagination
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.stream.Collectors

/**
 * Created by IntelliJ IDEA.
 * Project : simple-crud-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/03/21
 * Time: 20.57
 */
@Service
class StudentServiceImpl(
    val studentRepository: StudentRepository
) : StudentService {
    override fun create(student: Student): Student {
        studentRepository.save(student)
        return convertStudentResponse(student)
    }

    override fun get(id: String): Student {
        val student = findStudentById(id)
        return convertStudentResponse(student)
    }

    override fun update(id: String, updateStudent: Student): Student {
        val student = findStudentById(id)
        student.apply {
            name = updateStudent.name
            npm = updateStudent.npm
            email = updateStudent.email
        }
        studentRepository.save(student)
        return convertStudentResponse(student)
    }

    override fun delete(id: String) {
        val student = findStudentById(id)
        studentRepository.delete(student)
    }

    override fun list(listDataPagination: ListDataPagination): List<Student> {
        val page = studentRepository.findAll(PageRequest.of(listDataPagination.page, listDataPagination.size))
        val listStudent: List<Student> = page.get().collect(Collectors.toList())
        return listStudent.map {
            convertStudentResponse(it)
        }
    }

    private fun convertStudentResponse(student: Student): Student {
        return Student(
            id = student.id!!,
            name = student.name,
            npm = student.npm,
            email = student.email
        )
    }

    private fun findStudentById(id: String): Student {
        val student = studentRepository.findByIdOrNull(id)
        if (student == null) {
            throw Exception()
        } else {
            return student
        }
    }
}