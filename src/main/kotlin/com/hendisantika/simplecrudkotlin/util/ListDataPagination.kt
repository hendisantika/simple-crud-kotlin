package com.hendisantika.simplecrudkotlin.util

/**
 * Created by IntelliJ IDEA.
 * Project : simple-crud-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/03/21
 * Time: 20.56
 */
data class ListDataPagination(
    val page: Int,
    val size: Int
)