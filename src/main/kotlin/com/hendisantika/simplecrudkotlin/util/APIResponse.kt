package com.hendisantika.simplecrudkotlin.util

import org.springframework.http.HttpStatus

/**
 * Created by IntelliJ IDEA.
 * Project : simple-crud-kotlin
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/03/21
 * Time: 20.55
 */
data class APIResponse<T>(

    val code: Int,
    val status: HttpStatus,
    val data: T
)