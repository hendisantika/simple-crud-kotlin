# Simple CRUD Kotlin

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/simple-crud-kotlin.git`
2. Go inside the folder: `cd simple-crud-kotlin`
3. Make sure your MySQL is running well.
4. Run application: `gradle clean bootRun --info`

## Endpoint

### Create

Request:
`localhost:8080/api/students`

```json
{
  "name": "Uzumaki Naruto",
  "npm": "001",
  "email": "uzumaki_naruto@konohagakure.co.jp"
}
```

Response:

```json
{
  "code": 200,
  "status": "OK",
  "data": {
    "id": "7f505912-ee8f-461b-b873-0b6605da3d91",
    "name": "Uzumaki Naruto",
    "npm": "001",
    "email": "uzumaki_naruto@konohagakure.co.jp"
  }
}
```

### Get

`localhost:8080/api/students/id`

Response:

```json
{
  "code": number,
  "status": "string",
  "data": {
    "id": "UUID",
    "name": "string",
    "npm": "string",
    "email": "string"
  }
}
```

### Update

Request: `localhost:8080/api/students/id`

```json
{
  "name": "string",
  "npm": "string",
  "email": "string"
}
```

Response:

```json
{
  "code": number,
  "status": "string",
  "data": {
    "id": "UUID",
    "name": "string",
    "npm": "string",
    "email": "string"
  }
}
```

### List

`localhost:8080/api/students?size=10&page=0`

Response:

```json
{
  "code": number,
  "status": "string",
  "data": [
    {
      "id": "UUID",
      "name": "string",
      "npm": "string",
      "email": "string"
    }
  ]
}
```

### Delete

`Localhost:8080/api/students/id`

Response:

```json
{
  "code": number,
  "status": "string",
  "data": "string"
}
```